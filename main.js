//Synchronous javascript

// Synchronous means to be in a sequence. i.e every statement of the code gets executed one by one.

// ex
console.log("hii")
console.log("how")
console.log("are")
console.log("you")
console.log("doing")

// Asynchronous javascript

// Asynchronous code allows the program to be executed immediately where the synchronous code will block further execution of the remaining code until it finishes the current one. This may not look like a big problem but when you see it in a bigger picture you realize that it may lead to delaying the User Interface.

console.log("Hi");
  
 setTimeout(() => {
          console.log("Let us see what happens");
                 }, 3000);
  
    console.log("End")



    // Promises

    function func1() {
        return new Promise(function(resolve,reject){
        setTimeout(()=>{
            const error = true;
            if (!error){
                console.log("Your Promise has been resolved");
                resolve();
            }
            else{
                console.log("Your Promise has not been resolved");
                reject("Sorry! not fulfilled");
            }
        },2000)


        }
        )   
    }

    func1().then(function(){
        console.log("Thanks for resolving");
    }).catch(function(error){
        console.log("Too bad "+ ","+error);
    })


    // async and await

    async function API(){
        const url = "https://jsonplaceholder.typicode.com/users";
        const response = await fetch (url);
        const data = await response.json();
        return data;
    }
API().then(data=>{
    console.log(data);
})